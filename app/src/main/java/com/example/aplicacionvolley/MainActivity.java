package com.example.aplicacionvolley;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.aplicacionvolley.Views.Activity_Register;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    TextView txtRegistro;
    EditText edtCorreo, edtPassword;
    Button btnSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtCorreo = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtContra);
        btnSesion = findViewById(R.id.btnInicio);
        txtRegistro = findViewById(R.id.txtvRegistrarU);
        txtRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ints = new Intent(MainActivity.this, Activity_Register.class);
                startActivity(ints);
            }
        });
        btnSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtCorreo.getText().equals("") || edtPassword.getText().equals("")){
                    Toast.makeText(MainActivity.this, "No puede dejar los campos vacios", Toast.LENGTH_LONG).show();
                }else
                    Login(edtCorreo.getText().toString(), edtPassword.getText().toString());
            }
        });
    }

    public void Login(final String usuario, final String contra){
        StringRequest request = new StringRequest(Request.Method.POST, "https://notificacionupt.andocodeando.net/api/login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("true")){
                            Toast.makeText(MainActivity.this, "A iniciado sesion con exito", Toast.LENGTH_LONG).show();
                            Log.e("Respuesta", response.toString());
                        }else{
                            Toast.makeText(MainActivity.this, "Ocurrio un Error Al iniciar Sesion", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Ocurrio un error al conectarse", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", usuario);
                params.put("password", contra);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }
}