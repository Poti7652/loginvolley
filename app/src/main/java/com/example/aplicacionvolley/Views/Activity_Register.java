package com.example.aplicacionvolley.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.aplicacionvolley.MainActivity;
import com.example.aplicacionvolley.R;

import java.util.HashMap;
import java.util.Map;

public class Activity_Register extends AppCompatActivity {

    Button btnRegistro;
    EditText edtCorreo, edtContra, edtContra2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__register);
        btnRegistro = findViewById(R.id.btnRegistrar);
        edtCorreo = findViewById(R.id.edtEmail);
        edtContra = findViewById(R.id.edtPassword);
        edtContra2 = findViewById(R.id.edtPasswordRep);
        final TextView tvRegresar = findViewById(R.id.txtvRegresar);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtCorreo.getText().equals("") || edtContra.getText().equals("") || edtContra2.getText().equals(""))
                    Toast.makeText(Activity_Register.this, "No se puede dejar alguno de los campos vacios ", Toast.LENGTH_LONG).show();
                if(edtContra.getText().toString().equals(edtContra2.getText().toString())) {
                    Registro();
                }else
                    Toast.makeText(Activity_Register.this, "Las Contraseñas no Coinciden", Toast.LENGTH_LONG).show();
            }
        });
        tvRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity_Register.this, MainActivity.class));
                finish();
            }
        });
    }

    public void Registro(){
        StringRequest request = new StringRequest(Request.Method.POST, "https://notificacionupt.andocodeando.net/api/crearUsuario",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("true")){
                            Toast.makeText(Activity_Register.this, "Registro con exito", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(Activity_Register.this, MainActivity.class));
                            finish();
                        }else{
                            Toast.makeText(Activity_Register.this, "Fallo al realizar el Registro", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Activity_Register.this, "Ocurrio un error al conectarse", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", edtCorreo.getText().toString());
                params.put("password", edtContra.getText().toString());
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }
}